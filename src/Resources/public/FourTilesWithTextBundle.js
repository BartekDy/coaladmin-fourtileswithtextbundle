import React, {useEffect, useState} from 'react';
import EditForm from "./EditForm";
import HeadingWithTextComponent
    from "../../../assets/App/components/multi_use_components/HeadingWithTextComponent";
import TwoParagraphComponent
    from "../../../assets/App/components/multi_use_components/TwoParagraphComponent";
import ComponentMenu from "../../../assets/App/components/multi_use_components/ComponentMenu";
import SectionFormBase from "../../../assets/App/components/base_components/SectionFormBase";

const FourTilesWithTextBundle = ({params, id, components}) =>{
    const [firstTileFirstText, setFirstTileFirstText] = useState( '');
    const [firstTileSecondText, setFirstTileSecondText] = useState('');

    const [secondTileFirstText, setSecondTileFirstText] = useState('');
    const [secondTileSecondText, setSecondTileSecondText] = useState('');

    const [thirdTileFirstText, setThirdTileFirstText] = useState('');
    const [thirdTileSecondText, setThirdTileSecondText] = useState('');

    const [fourthTileFirstText, setFourthTileFirstText] = useState('');
    const [fourthTileSecondText, setFourthTileSecondText] = useState('');

    const title = 'Edycja Sekcji 2'

    const [showEditForm, setShowEditForm] = useState(false)
    useEffect(() => {
        params[0].elements.map((el=> {
            switch (el.name) {
                case 'Pierwszy kafelek':
                    setFirstTileFirstText(el.heading)
                    return setFirstTileSecondText(el.text)
                case 'Drugi kafelek':
                    setSecondTileFirstText(el.heading)
                    return setSecondTileSecondText(el.text)
                case 'Trzeci kafelek':
                    setThirdTileFirstText(el.heading)
                    return setThirdTileSecondText(el.text)
                case 'Czwarty kafelek':
                    setFourthTileFirstText(el.heading)
                    return setFourthTileSecondText(el.text)
                default:
                    return null
            }
        }))
    },[])

    return(
        <>
            <section className={'FourTilesWithTextBundle'} style={{display:"flex"}}>
                <div style={{display:"flex"}}>
                    <HeadingWithTextComponent  heading={firstTileFirstText}  text={firstTileSecondText}  containerClassName={'First-Tile'} />
                </div>
                <TwoParagraphComponent firstParagraph={secondTileFirstText} secondParagraph={secondTileSecondText} componentClassName ={"Second-Tile"} />
                <TwoParagraphComponent firstParagraph={thirdTileFirstText} secondParagraph={thirdTileSecondText} componentClassName ={"Third-Tile"} />
                <TwoParagraphComponent firstParagraph={fourthTileFirstText} secondParagraph={fourthTileSecondText} componentClassName ={"Fourth-Tile"} />
                <ComponentMenu id={id} components={components} editFunction={e=>setShowEditForm(true)}/>
            </section>
            {showEditForm === true ? <SectionFormBase closeBtnFucntion={e => setShowEditForm(false)}
                                                      title={title}
                                                      formContainerClass={''}
                                                      form={<EditForm id={id}/>}
            /> : null }
        </>
    )
}

export default FourTilesWithTextBundle;