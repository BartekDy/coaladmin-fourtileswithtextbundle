import React, {useRef, useState, useEffect} from 'react';
import {useForm} from "react-hook-form";
import EditSection from "../../../assets/js/EditSection"


const EditForm = ({id}) =>{

    const {register, control, formState: {errors}} = useForm();

    const [firstTileFirstText, setFirstTileFirstText] = useState(null);
    const [firstTileSecondText, setFirstTileSecondText] = useState(null);

    const [secondTileFirstText, setSecondTileFirstText] = useState(null);
    const [secondTileSecondText, setSecondTileSecondText] = useState(null);

    const [thirdTileFirstText, setThirdTileFirstText] = useState(null);
    const [thirdTileSecondText, setThirdTileSecondText] = useState(null);

    const [fourthTileFirstText, setFourthTileFirstText] = useState(null);
    const [fourthTileSecondText, setFourthTileSecondText] = useState(null);

    const form = useRef(null);

    useEffect(() => {
        fetch(`${window.location.origin}/api/page_sections/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                return response.json()
            })
            .then(data => {
                data.params[0].elements.map((el=> {
                    switch (el.name) {
                        case 'Pierwszy kafelek':
                            setFirstTileFirstText(el.heading)
                            return  setFirstTileSecondText(el.text)
                        case 'Drugi kafelek':
                            setSecondTileFirstText(el.heading)
                            return setSecondTileSecondText(el.text)
                        case 'Trzeci kafelek':
                            setThirdTileFirstText(el.heading)
                            return  setThirdTileSecondText(el.text)
                        case 'Czwarty kafelek':
                            setFourthTileFirstText(el.heading)
                            return  setFourthTileSecondText(el.text)
                        default:
                            return null
                    }
                }))
            })
            .catch((error) => {
            });
    },[])

    const onSubmit = (e) => {
        e.preventDefault()
        const data = [
            {
                elements: [
                    {
                        id: 1,
                        name: "Pierwszy kafelek",
                        heading: firstTileFirstText,
                        text: firstTileSecondText
                    },
                    {
                        id: 2,
                        name: "Drugi kafelek",
                        heading: secondTileFirstText,
                        text: secondTileSecondText
                    },
                    {
                        id: 3,
                        name: "Trzeci kafelek",
                        heading: thirdTileFirstText,
                        text: thirdTileSecondText
                    },
                    {
                        id: 4,
                        name: "Czwarty kafelek",
                        heading: fourthTileFirstText,
                        text: fourthTileSecondText
                    },
                ]
            }
        ]

        EditSection(id,data);
    }
    return(
        <form method={'POST'} ref={form} className='add_section_text_with_image' onSubmit={onSubmit}>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" name="firstTileFirstText" {...register("firstTileFirstText")}
                       value={firstTileFirstText} onChange={(e) => setFirstTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Treść</label>
                <input type="text" name="firstTileSecondText" {...register("firstTileSecondText")}
                       value={firstTileSecondText} onChange={(e) => setFirstTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <div className="form-control">
                <label>Opinia</label>
                <input type="text" name="secondTileFirstText" {...register("secondTileFirstText")}
                       value={secondTileFirstText} onChange={(e) => setSecondTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Imię i nazwisko</label>
                <input type="text" name="secondTileSecondText" {...register("secondTileSecondText")}
                       value={secondTileSecondText} onChange={(e) => setSecondTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <div className="form-control">
                <label>Opinia</label>
                <input type="text" name="thirdTileFirstText" {...register("thirdTileFirstText")}
                       value={thirdTileFirstText} onChange={(e) => setThirdTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Imię i nazwisko</label>
                <input type="text" name="thirdTileSecondText" {...register("thirdTileSecondText")}
                       value={thirdTileSecondText} onChange={(e) => setThirdTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <div className="form-control">
                <label>Opinia</label>
                <input type="text" name="fourthTileFirstText" {...register("fourthTileFirstText")}
                       value={fourthTileFirstText} onChange={(e) => setFourthTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Imię i nazwisko</label>
                <input type="text" name="fourthTileSecondText" {...register("fourthTileSecondText")}
                       value={fourthTileSecondText} onChange={(e) => setFourthTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <button id="submit" type="submit" className="btn btn-block"><i
                className="far fa-save"> </i> Zapisz
            </button>
        </form>
    )
}
export default EditForm;