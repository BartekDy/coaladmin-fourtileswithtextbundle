<?php
namespace FourTilesWithTextBundle\Command;

use App\Entity\Section;
use App\Repository\SectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FourTilesWithTextBundleCommand extends Command
{
    private $em;
    private $sectionRepository;

    public function __construct(EntityManagerInterface $em, SectionRepository $sectionRepository)
    {
        $this->em = $em;
        $this->sectionRepository = $sectionRepository;

        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'coaladmin:create-FourTilesWithTextBundle';

    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new section with for tiles with text inside.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $section = $this->sectionRepository->findOneBy(['title' => 'FourTilesWithTextBundle']);

        if (!$section) {
            $section = new Section();
        }

        $section
            ->setTitle('FourTilesWithTextBundle')
            ->setParams($this->getParams())
            ->setTemplateUrl('@FourTilesWithTextBundle/template/show.html.twig')
        ;

        $this->em->persist($section);
        $this->em->flush();

        return Command::SUCCESS;
    }

    private function getParams()
    {
        $params = [
            'elements'=> [
                [
                    "id" => 1,
                    "name" => "Pierwszy kafelek",
                    "heading" => null,
                    'text' => null
                ],
                [
                    "id" => 2,
                    "name" => "Drugi kafelek",
                    "heading" => null,
                    'text' => null
                ],
                [
                    "id" => 3,
                    "name" => "Trzeci kafelek",
                    "heading" => null,
                    'text' => null
                ],
                [
                    "id" => 4,
                    "name" => "Czwarty kafelek",
                    "heading" => null,
                    'text' => null
                ],

            ]
        ];

        return $params;
    }
}