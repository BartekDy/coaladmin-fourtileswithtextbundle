<?php

namespace FourTilesWithTextBundle;

use FourTilesWithTextBundle\DependencyInjection\FourTilesWithTextBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FourTilesWithTextBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new FourTilesWithTextBundleExtension();
        }
        return $this->extension;
    }
}