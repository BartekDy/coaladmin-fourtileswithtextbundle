**Installation**  
**1.** add this to composer.json
```
    "repositories": [  
        {"type": "composer", "url": "https://pelkas.repo.repman.io"}  
    ]  
```
**2.** composer require coaladmin/four-tiles-with-text-bundle  
**3.** bin/console coaladmin:create-FourTilesWithTextBundle 
**4.** bin/console assets:install

**Przykład**
![](src/Resources/public/ExamplePicture.png)